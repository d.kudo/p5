let a=100; //カロリー オブジェクト 0-500
let b=75; //心拍 オブジェクト50-160
let c=100; //睡眠スコア 背景カラー上 0-100
let d=c * 0.6; //睡眠スコア 背景カラー下 0-100
let e=30; //活動スコア オブジェクトカラー 0-100

var c1, c2; // Gradation

let s = 45; //背景彩度
let ss = 40; //オブジェクト彩度
let k = c * 0.6;

let x;
let y;
let angle = 0.0;
let jitter = 0.0;

function setup() {
  createCanvas(windowWidth, windowHeight);
  colorMode(HSB, 360, 100, 100, 100);
  
  if(a >= 500){
  a=500;
  }else if(a <= 30) {
  a=30;
  }
  f=Math.round(a/10);
  
  
  if(c <=0){
    c=0;
  }else if(c >=100){
    c=100;
  }
  h=100-c;
  i=58+2.12*h;
  if(d <=52){
  i2=i-100;
  }else if(d >=53){
    h2=d-53;
    i2=269.64-2.12*h2;
  }
  
  // if(e >= 83) {
  // e=83;
  // }else if(e <= 6){
  // e=6;
  // }
  
  x = 0;
  y = 0;
}

//Setup Gradient
function setGradient(x, y, w, h, c1, c2) {
  noFill();
  for (var i = y; i < h; i++) {
      var c = lerpColor(c1, c2, i * 0.4 / w - 0.2);
      stroke(c);
      line(x, i, w, i);
  }
}

function draw() {
  //background Gradient
   let color1 = color(270 - c * 2.12,s,95);
   let color2 = color(280 - c,s,45);
    if(c <= 25){
        s = 30;
    }
    setGradient(0,0,width,height * 0.2,color1,color2);
    setGradient(0,height * 0.2,width,height,color1,color2);
    
  translate(width/2, height / 3);

  stroke(color(0,0,100,20));
  strokeWeight(1);
  
  // noStroke();
  // noFill();
  fill(270-e*2.12, ss, 80, 20);
    if(e >= 100){
                    ss = 80;
                }
                if(c >= 90){
                    ss = 80;
                }
  z=(sin(frameCount/150));
  rotate(z);
  
  drawTriangleStrip(f);

function drawTriangleStrip(numPoints) {
    let angle = 1;
    let angleStep = 360.0 / numPoints / 3;

  beginShape(TRIANGLE_STRIP);
  for (let i = 0; i <= numPoints; i++) {
    let px = x + cos(radians(angle)) * (sin(frameCount/200*b*0.003)*300);
    let py = y + sin(radians(angle)) * (sin(frameCount/200*b*0.003)*300);
    angle += angleStep;
    vertex(px, py);
    px = x + cos(radians(angle)) * (sin(frameCount/130*b*0.03)*300);
    py = y + sin(radians(angle)) * (sin(frameCount/130*b*0.03)*300);
    vertex(px, py);
    angle += angleStep;
    px = x + cos(radians(angle)) * (sin(frameCount/60*b*0.003)*300);
    py = y + sin(radians(angle)) * (sin(frameCount/60*b*0.003)*300);
    vertex(px, py);
    angle += angleStep;
  }
  endShape();
}
}
